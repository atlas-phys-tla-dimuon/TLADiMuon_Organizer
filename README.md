# TLA DiMuon Analysis Workspace
Workspace for running the DiMuon TLA analysis on xAOD's.

## Repository Structure

- `source/` Requried packages outside of the standard athena release.

## Setup Instructions

The following commands will build the workspace after making a recursive clone
of this workspace:

```sh
asetup 22.2.58,AthAnalysis
cmake -Ssource -Bbuild -DATLAS_PACKAGE_FILTER_FILE=source/package_filters.txt
source build/${AthAnalysis_PLATFORM}/setup.sh
cmake --build build/
```

## Example Commands

### Making Ntuples

```sh
lad.sh TLADiMuon/ntuples_jobOptions.py ${MYWORKSPACE}/source/TLADiMuon/filelists/Signal_MG5Py8EG.AOD.list
```
